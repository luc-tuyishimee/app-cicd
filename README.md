![pipeline status](https://gitlab.com/luc-tuyishimee/app-cicd/badges/master/pipeline.svg)

# CI/CD pipelines for a React APP and deploy to Netlify.

### Setting up a Gitlab account

- [Create account](https://about.gitlab.com/)
- Create an empty project

## Setting up a React project

```
npx create-react-app my-app
cd my-app
npm start or yarn start
```

## Run the unit test

```
npm test
```

## Run test automatically

```
CI=true npm test
```

## Add a gitlab yml file [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/README.html)

- Paste this code inside your .gitlab-ci.yml file

```
image: node:12.17.0

stages:
  - test
  - build
  - deploy

test:
  stage: test
  script:
    - npm install
    - CI=true npm test

build:
  stage: build
  script:
    - npm install
    - npm run build
  artifacts:
    paths:
      - build

.deploy_dependencies: &deploy_dependencies
  stage: deploy
  before_script:
    - npm install -g netlify-cli
  only:
    - tags
    - master

deploy_preview_prod:
  <<: *deploy_dependencies
  environment:
    name: production-preview

  script:
    - echo "Deploying preview"
    - netlify deploy --dir=build

deploy_production_prod:
  <<: *deploy_dependencies
  environment:
    name: production
  script:
    - echo "Deploying productions"
    - netlify deploy --prod --dir=build
  when: manual
```

## Remove the git repository from facebook

```
rm -rf .git
```

## Create your Own repository

```
git init
git add .
git status
git commit -m "initial commit"
git remote url from Gitlab to push to an existing folder
git push origin master
```

# Deploying manually

## Install netlify CLI

```
npm i -g netlify-cli

```

## Create an access token

```

* [Netlify website](https://app.netlify.com/)
* User settings
* Application
* New access token
```

## Authenticate CLI

- Replace token with the value you got when creating the token.

```
export NETLIFY_AUTH_TOKEN=TOKEN
```

## Create the site where to deploy the APP

```
netlify sites:create --name name-of-the-app
```

## Specify the Site ID

```
export NETLIFY_SITE_ID=SITE_ID
```

## Deploy

```
netlify deploy --prod --dir=build
```

# Deploying automatically (CD)

- In your Gitlab repository
- Go to setting
- Click on expand on the variable tab
- Key => NETLIFY_SITE_ID
- Value => SITE_ID
- push your changes and check you pipelines

[Link to the deployed APP](https://my-new-app.netlify.app/)
