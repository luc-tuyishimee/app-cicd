import React from "react";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">CI-CD react with Gitlab.</header>
    </div>
  );
}

export default App;
